package com.example.airstrikeexample;

import com.example.airstrikeexample.bean.StrategyObject;
import com.example.airstrikeexample.db.AirSrikeHelper;
import com.example.airstrikeexample.db.StrategyDataContentProvider;
import com.example.airstrikeexample.views.GameView;
import com.example.airstrikeexample.views.UperLayerOfGameView;

import android.app.Activity;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;

public class GameActivity extends Activity{
	
	GameView myGameView;
	UperLayerOfGameView upperLayer;
	ImageView blastImg;
	Animation hyperspaceJumpAnimation;
	Drawable d;
	int finalHeight,finalWidth;
	SparseArray<StrategyObject> objectsPlaned;
	StrategyDataContentProvider objectPositionProvider;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.game_activity_layout);
		
		myGameView	= (GameView)findViewById(R.id.game_img_view);
		blastImg	= (ImageView)findViewById(R.id.blast_img);
		blastImg.setVisibility(View.GONE);
		
		d				= getResources().getDrawable(R.drawable.blast_img1);
		finalHeight 	= d.getIntrinsicHeight(); 
		finalWidth		= d.getIntrinsicWidth(); 
		objectsPlaned	= new SparseArray<StrategyObject>();
		
		objectPositionProvider	= new StrategyDataContentProvider(GameActivity.this);
		Cursor c				= objectPositionProvider.query(AirSrikeHelper.STRATEGY_CONTENT_URI, null, null, null, null);
		while(c.moveToNext()){
			StrategyObject obj = new StrategyObject();
			obj.setStartRow(c.getInt(c.getColumnIndex(StrategyDataContentProvider.START_ROW)));
			obj.setEndRow(c.getInt(c.getColumnIndex(StrategyDataContentProvider.END_ROW)));
			obj.setStartColumn(c.getInt(c.getColumnIndex(StrategyDataContentProvider.START_COLUMN)));
			obj.setEndColumn(c.getInt(c.getColumnIndex(StrategyDataContentProvider.END_COLUMN)));
			objectsPlaned.append(c.getInt(c.getColumnIndex(StrategyDataContentProvider.ID)), obj);
		}
		
		Log.e("test","object list size:"+objectsPlaned.size());
		
		hyperspaceJumpAnimation = AnimationUtils.loadAnimation(this, R.anim.test_animation);
		hyperspaceJumpAnimation.setAnimationListener(new AnimationListener() {
			
			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				blastImg.setVisibility(View.GONE);
			}
		});
		
		myGameView.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				switch(event.getAction()){
					case MotionEvent.ACTION_DOWN:
						
						if(myGameView.isTouchInBox((int)event.getX(), (int)event.getY())){
							
							int row		= myGameView.getRow((int)event.getY())+1;
							int column	= myGameView.getColumn((int)event.getX())+1;
							
							Log.e("test game","Row:"+row);
							Log.e("test game","Column:"+column);
							
							for (int i = 0; i < objectsPlaned.size(); i++) {
								int sRow	= objectsPlaned.get(objectsPlaned.keyAt(i)).getStartRow();
								int eRow	= objectsPlaned.get(objectsPlaned.keyAt(i)).getEndRow();
								int sColumn	= objectsPlaned.get(objectsPlaned.keyAt(i)).getStartColumn();
								int eColumn	= objectsPlaned.get(objectsPlaned.keyAt(i)).getEndColumn();
								
								if((row>=sRow) && (row<=eRow) && (column>=sColumn) && (column<=eColumn)){
									int size = myGameView.getBoxSize();
									LayoutParams params = new LayoutParams(size*2, size*2);
									
									int x = (int)event.getX()-(size);
									int y = (int)event.getY()-(size);
									blastImg.setImageDrawable(d);
									params.setMargins(x, y, 0, 0);
									blastImg.setLayoutParams(params);
									blastImg.setVisibility(View.VISIBLE);
									blastImg.startAnimation(hyperspaceJumpAnimation);
								}
								
							}
							
						}
						return true;
				}
				
				return false;
			}
		});
	
	}
}
