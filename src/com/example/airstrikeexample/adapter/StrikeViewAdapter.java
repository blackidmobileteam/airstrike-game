package com.example.airstrikeexample.adapter;

import java.util.ArrayList;

import com.example.airstrikeexample.R;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;

public class StrikeViewAdapter extends BaseAdapter{
	
	ArrayList<Integer> strikeImages;
	Context context;
	LayoutInflater inflater;
	
	public StrikeViewAdapter(Context con,ArrayList<Integer> imgs) {
		
		context			= con;
		strikeImages	= imgs;
		inflater		= (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return strikeImages.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return strikeImages.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		convertView		= inflater.inflate(R.layout.grid_view_item, parent, false);
		
		ImageView m1	= (ImageView)convertView.findViewById(R.id.grid_item);
		m1.setImageResource(strikeImages.get(position));
		
		int height		= parent.getHeight();
		
		int imgsize		= height/4;
		
		LayoutParams params	= new LayoutParams(imgsize, imgsize);
		m1.setLayoutParams(params);
		
		return convertView;
	} 

}
