package com.example.airstrikeexample.adapter;

import java.util.ArrayList;

import com.example.airstrikeexample.MainActivity;
import com.example.airstrikeexample.R;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;

public class ImageGalleryAdapter extends BaseAdapter{
	
	static Context context;
	static ArrayList<Integer> imageIds;
	LayoutInflater inflater;
	
	ImageView galleryImg;
	
	public ImageGalleryAdapter(Context con,ArrayList<Integer> imgIds) {
		// TODO Auto-generated constructor stub
		context		= con;
		imageIds	= imgIds;
		inflater	= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return imageIds.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return imageIds.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@SuppressLint("NewApi")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		final int p = position;
		
		convertView	= inflater.inflate(R.layout.list_item_view, parent, false);
		galleryImg	= (ImageView)convertView.findViewById(R.id.gallery_img);
		int size	= ((MainActivity) context).getSquareSize();
		galleryImg.setImageResource(imageIds.get(position));
		
		galleryImg.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				int size	= ((MainActivity) context).getSquareSize();
				
				Log.i("test","in adapter size:"+size);
				
				ClipData.Item item 	= new ClipData.Item(""+p);
				String[] mimeTypes 	= { ClipDescription.MIMETYPE_TEXT_PLAIN };
				ClipData data 		= new ClipData("", mimeTypes, item);
				MyDragShadowBuilder shadowBuilder = new MyDragShadowBuilder(v, size, p, (int)event.getX(), (int)event.getY());
				v.startDrag(data, shadowBuilder, v, 0);
			    
				return false;
			}
		});
		
		return convertView;
	}
	
	static class MyDragShadowBuilder extends View.DragShadowBuilder{
		
		int boxSize,index;
		int positionX,positionY;
		private static Drawable shadow;
		
		public MyDragShadowBuilder(View v,int size,int index,int x,int y){
			super(v);
			shadow		= context.getResources().getDrawable(imageIds.get(index));
			boxSize		= size;
			this.index	= index;
			positionX	= x;
			positionY	= y;
		}
		
		@Override
		public void onProvideShadowMetrics(Point shadowSize,Point shadowTouchPoint) {
			// TODO Auto-generated method stub
			Log.e("test","Box Size"+boxSize);
			shadow.setBounds(0, 0, boxSize, boxSize*(2+index));
			shadowSize.set(boxSize, boxSize*(2+index));
			shadowTouchPoint.x	= boxSize/2;
			shadowTouchPoint.y 	= (boxSize*(2+index))/2;
		}
		
		@Override
		public void onDrawShadow(Canvas canvas) {
			shadow.draw(canvas);
		}
		
	}
	
}
