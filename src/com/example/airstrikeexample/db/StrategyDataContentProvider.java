package com.example.airstrikeexample.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

public class StrategyDataContentProvider extends ContentProvider{

	Context context;
	
	static final String TABLE_NAME = "strategy_master";
	private SQLiteDatabase db;
	private AirSrikeHelper dbHelper;
	
	public static String ID				= "id";
	public static String START_ROW 		= "obj_start_row";
	public static String END_ROW 		= "obj_end_row";
	public static String START_COLUMN 	= "obj_start_column";
	public static String END_COLUMN 	= "obj_end_column";
	
	public StrategyDataContentProvider(Context con){
		context		= con;
		dbHelper	= new AirSrikeHelper(context);
	}
	
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		db = dbHelper.getWritableDatabase();    
		int count = db.delete(TABLE_NAME, selection, selectionArgs);  
		context.getContentResolver().notifyChange(uri, null);  
		Log.i("DELETE","DELETE AIRPORT RECORD");
		db.close();
		return count;
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		Uri _uri = null;
		db = dbHelper.getWritableDatabase();
		long rowID = db.insert(TABLE_NAME, "", values);
		if (rowID > 0) {
			_uri = ContentUris.withAppendedId(AirSrikeHelper.STRATEGY_CONTENT_URI, rowID);
            context.getContentResolver().notifyChange(_uri, null);    
        }
		if(_uri != null){
			Log.i("Test", "Data Inserted Successfully");
		}
        db.close();
		return _uri;
	}

	@Override
	public boolean onCreate() {
		Context context = getContext();
	    AirSrikeHelper dbHelper = new AirSrikeHelper(context);
	    db = dbHelper.getWritableDatabase();
	    db.close();
	    return (db == null)? false:true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		Cursor c = null;
		
		SQLiteQueryBuilder qb_airport = new SQLiteQueryBuilder();
		db = dbHelper.getReadableDatabase();
		qb_airport.setTables(TABLE_NAME);
	    c = qb_airport.query(db, projection, selection, selectionArgs, null, sortOrder, sortOrder);
	    c.setNotificationUri(context.getContentResolver(), uri);
	    return c;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		SQLiteDatabase db_deck = dbHelper.getWritableDatabase();    
		int count = db_deck.update(TABLE_NAME, values, selection, selectionArgs);  
		context.getContentResolver().notifyChange(uri, null); 
		db.close();
		return count;
	}

}
