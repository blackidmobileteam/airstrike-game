package com.example.airstrikeexample.db;

import android.content.Context;
import android.content.UriMatcher;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

public class AirSrikeHelper extends SQLiteOpenHelper{
	
	private static final int DATABASE_VERSION 		= 1;
	private static final String DATABASE_NAME 		= "AirstikeDB";
	
	static final String PROVIDER_NAME 				= "com.example.airstrikeexample.db";
	public static String STRATEGY_MASTER_URL 		= "content://"+PROVIDER_NAME+"/strategy_master";
	public static final Uri STRATEGY_CONTENT_URI 	= Uri.parse(STRATEGY_MASTER_URL);
	
	private static final int STRATEGY_URI_CODE 		= 1;
	private static UriMatcher uriMacher 			= null; 
	
	static {
		uriMacher	= new UriMatcher(UriMatcher.NO_MATCH);
		uriMacher.addURI(PROVIDER_NAME, STRATEGY_MASTER_URL, STRATEGY_URI_CODE);
	}
	
	String CREATE_STRATEGY_MASTER = "CREATE TABLE strategy_master ("+
			"id INTEGER PRIMARY KEY AUTOINCREMENT, "+
			"obj_start_row INTEGER, "+
			"obj_end_row INTEGER, "+
			"obj_start_column INTEGER, "+
			"obj_end_column INTEGER);";
	/*static final String CREATE_STRATEGY_MASTER =
			"CREATE TABLE strategy_master ("
			+ "deck_id INTEGER PRIMARY KEY AUTOINCREMENT,"
			+"obj_start_row INTEGER, "
			+"obj_end_row INTEGER, "
			+"obj_start_column INTEGER,"
			+"obj_end_column INTEGER"
			+ ");";*/
	
	public AirSrikeHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_STRATEGY_MASTER);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}
}
