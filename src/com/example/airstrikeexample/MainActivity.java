package com.example.airstrikeexample;

import java.util.ArrayList;

import com.example.airstrikeexample.adapter.ImageGalleryAdapter;
import com.example.airstrikeexample.db.AirSrikeHelper;
import com.example.airstrikeexample.db.StrategyDataContentProvider;
import com.example.airstrikeexample.utils.MyImageView;
import com.example.airstrikeexample.views.GameView;

import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.util.Log;
import android.view.DragEvent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnClickListener;
import android.view.View.OnDragListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

public class MainActivity extends Activity {
	
	TextView xCoordinate;
	TextView yCoordinate;
	LinearLayout l1;
	
	ListView imageGallery;
	GameView myGameView;
	RelativeLayout myCustomView;
	Button strategyDone;
	
	int size,xStart;
	ArrayList<Integer> imageIds;
	ImageGalleryAdapter galleryAdapter;
	ArrayList<View> strikeImages;
	StrategyDataContentProvider positionDataProvider;
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		imageGallery		= (ListView)findViewById(R.id.img_gallery);
		myGameView			= (GameView)findViewById(R.id.game_img_view);
		myCustomView		= (RelativeLayout)findViewById(R.id.mycustom_view);
		strategyDone		= (Button)findViewById(R.id.done_btn);
		
		imageIds			= new ArrayList<Integer>();
		strikeImages		= new ArrayList<View>();
		positionDataProvider= new StrategyDataContentProvider(MainActivity.this);
		
		imageIds.add(R.drawable.fisher_price_plane_120);
		imageIds.add(R.drawable.toyplane1_120);
		imageIds.add(R.drawable.toyplane2_120);
		imageIds.add(R.drawable.toyplane3_120);
		/*imageIds.add(R.drawable.toyplane4_120);
		imageIds.add(R.drawable.toyplane5_120);*/
		//Display display = getWindowManager().getDefaultDisplay(); 
		//height = display.getHeight();  // deprecated
		
		galleryAdapter	= new ImageGalleryAdapter(MainActivity.this, imageIds);
		imageGallery.setAdapter(galleryAdapter);
		
		strategyDone.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				positionDataProvider.delete(AirSrikeHelper.STRATEGY_CONTENT_URI, null, null);
				for(int i=0;i<myCustomView.getChildCount();i++){
					View relativeView = myCustomView.getChildAt(i);
					
					if(relativeView instanceof MyImageView && relativeView.getVisibility()==View.VISIBLE){
						ContentValues values = new ContentValues();
						values.put(StrategyDataContentProvider.START_ROW	, ((MyImageView) relativeView).getStartRow());
						values.put(StrategyDataContentProvider.END_ROW		, ((MyImageView) relativeView).getEndRow());
						values.put(StrategyDataContentProvider.START_COLUMN	, ((MyImageView) relativeView).getStartColumn());
						values.put(StrategyDataContentProvider.END_COLUMN	, ((MyImageView) relativeView).getEndColumn());
						positionDataProvider.insert(AirSrikeHelper.STRATEGY_CONTENT_URI, values);
					}
				}
				Intent intent	= new Intent(MainActivity.this,GameActivity.class);
				startActivity(intent);
			}
		});
		
		//imageGallery.setOnDragListener(new MyDragListener());
		myGameView.setOnDragListener(new MyDragListener());
		
	}
	public int getStatusBarHeight() {
	    Rect r = new Rect();
	    Window w = getWindow();
	    w.getDecorView().getWindowVisibleDisplayFrame(r);
	    return r.top;
	}
	 
	public int getTitleBarHeight() {
	    int viewTop = getWindow().findViewById(Window.ID_ANDROID_CONTENT).getTop();
	    return (viewTop - getStatusBarHeight());
	}
	@SuppressLint("NewApi")
	class MyDragListener implements OnDragListener {
		
		@Override
		public boolean onDrag(View v, DragEvent event) {
			// TODO Auto-generated method stub
			View view = (View)event.getLocalState();

			switch (event.getAction()) {
		    case DragEvent.ACTION_DRAG_STARTED:
		    // do nothing
		    	if(v==myGameView && (view instanceof MyImageView)){
		    		view.setVisibility(View.GONE);
		    	}
		      return true;
		    case DragEvent.ACTION_DRAG_ENTERED:
		    	return true;
		    case DragEvent.ACTION_DRAG_EXITED:         
		    	return true;
		    case DragEvent.ACTION_DROP:
		      // Dropped, reassign View to ViewGroup
		    	
		    	if(v==myGameView && (view instanceof ImageView) && !(view instanceof MyImageView)){
		    		//Log.e("test","in main drag");
		    		String indexStr	= event.getClipData().getItemAt(0).getText().toString();
		    		
		    		if(indexStr.equalsIgnoreCase("")){
		    			return false;
		    		}
		    		
		    		int index			= Integer.parseInt(indexStr);
		    		
		    		if(!myGameView.isImageInBox((int)event.getX(),(int)event.getY(),index)){
		    			return false;
		    		}
		    		
		    		size				= getSquareSize();
	    			int l				= (int)event.getX();
			    	l					= l-(size/2);
			    	int t				= (int)event.getY();
			    	t					= t-((size*(2+index))/2);
			    	
			    	int r				= myGameView.getRow(t);
			    	int c				= myGameView.getColumn(l);
		    		
	    			if(!checkPosition(r+1, c+1, index)){
	    				return false;
	    			}
	    			
			    	xStart				= myGameView.getXStart();
			    	int left			= 0;
			    	if(c==0){
			    		left			= xStart;
					}else{
						left			= xStart+(size*c);
					}
			    	int top				= 0;
					if(r==0){
						top				= 10;
					}else{
						top				= 10+(size*r);
					}
					
			    	Log.e("test","row:"+r);
			    	Log.e("test","column:"+c);
			    	int startRow		= r+1;
			    	int endRow			= startRow+((2+index)-1);
			    	if(endRow>8 || endRow<1){
			    		return false;
			    	}
			    	
			    	Bitmap bmp 			= BitmapFactory.decodeResource(getResources(), imageIds.get(index));
			    	
			    	Bitmap b			= Bitmap.createScaledBitmap(bmp, size, size*(2+index), false);
			    	LayoutParams param	= new LayoutParams(size,size*(2+index));
			    	
			    	MyImageView v1		= new MyImageView(MainActivity.this, index);
			    	
			    	v1.setStartRow(startRow);
			    	v1.setEndRow(startRow+((2+index)-1));
			    	v1.setStartColumn(c+1);
			    	v1.setEndColumn(c+1);
			    	
			    	v1.setImageBitmap(b);
			    	
			    	param.setMargins(left,top, 0, 0);
			    	
			    	v1.setLayoutParams(param);

			    	
			    	v1.setOnTouchListener(new OnTouchListener() {
						
						@Override
						public boolean onTouch(View v, MotionEvent event) {
							// TODO Auto-generated method stub
							
							ClipData.Item item 	= new ClipData.Item("");
							String[] mimeTypes 	= { ClipDescription.MIMETYPE_TEXT_PLAIN };
							ClipData data 		= new ClipData("", mimeTypes, item);

						    DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);
						    v.startDrag(data, shadowBuilder, v, 0);
							
							return false;
						}
					});
			    	
			    	v1.setOnLongClickListener(new OnLongClickListener() {
						
						@Override
						public boolean onLongClick(View v) {
							// TODO Auto-generated method stub
							v.setVisibility(View.GONE);
							return false;
						}
					});
			    	
			    	//Log.e("test","Size:"+size);
			    	
			    	myCustomView.addView(v1);
				    
			    	return true;
		    	}else if(v==myGameView && (view instanceof MyImageView)){
		    		
		    		//Log.e("test","in image drag");
		    		size				= myGameView.getBoxSize();
		    		MyImageView imgView = (MyImageView)view;
	    			int imgID			= imgView.getBitmapIndex();
		    		
	    			if(!myGameView.isImageInBox((int)event.getX(), (int)event.getY(),imgID)){
	    				imgView.setVisibility(View.VISIBLE);
	    				Log.e("test","in condition");
	    				return true;
	    			}
	    			
		    		int l				= (int)event.getX();
		    		l					= l-(size/2);
			    	int t				= (int)event.getY();
			    	t					= t-((size*(2+imgID))/2);
			    	
			    	int height			= 0;
			    	int r				= myGameView.getRow(t);
			    	int c				= myGameView.getColumn(l);
			    	
			    	if(r<0 || c<0){
			    		view.setVisibility(View.VISIBLE);
			    		return false;
			    	}
			    	
			    	if(!checkPosition(r, c,imgID)){
			    		view.setVisibility(View.VISIBLE);
			    		return false;
			    	}
			    	
			    	/*Log.e("test","droped image x:"+l);
			    	Log.e("test","droped image x+size:"+(l+size));*/
			    	
			    	int left			= 0;
			    	if(c==0){
			    		left			= xStart;
					}else{
						left			= xStart+(size*c);
					}
			    	int top				= 0;
					if(r==0){
						top				= 10;
					}else{
						top				= 10+(size*r);
					}
			    	
					int endRow			= r+1+((2+imgID)-1);
					
					if(endRow>8 || endRow<1){
						view.setVisibility(View.VISIBLE);
			    		return false;
					}
					
	    			LayoutParams param	= new LayoutParams(size,size*(2+imgID));
			    	
			    	param.setMargins(left,top, 0, 0);
			    	
			    	imgView.setLayoutParams(param);
			    	imgView.setStartRow(r+1);
			    	imgView.setEndRow(r+1+((2+imgID)-1));
			    	imgView.setStartColumn(c+1);
			    	imgView.setEndColumn(c+1);
			    	view.setVisibility(View.VISIBLE);
		    		
			    	return true;
		    	}else if(v==imageGallery && (view instanceof MyImageView)){
		    		view.setVisibility(View.VISIBLE);
		    		return true;
		    	}
		    	
		    case DragEvent.ACTION_DRAG_ENDED:
		    	return true;
		      
		    default:
		      break;
		    }
		    return false;
		}

	}
	
	public int getSquareSize(){
		int s	= myGameView.getBoxSize();
		return s;
	}
	
	public boolean checkPosition(int row,int column,int index){
		
		int startRow	= row+1;
		int startColumn	= column+1;
		int endRow		= startRow+((2+index)-1);
		
		/*Log.e("test","start row:"+startRow);
		Log.e("test","end row:"+endRow);
		Log.e("test","column:"+startColumn);*/
		
		for(int i=0;i<myCustomView.getChildCount();i++){
			View relativeView = myCustomView.getChildAt(i);
			
			if(relativeView instanceof MyImageView && relativeView.getVisibility()==View.VISIBLE){
				int viewStartRow		= ((MyImageView) relativeView).getStartRow();
				int viewEndRow			= ((MyImageView) relativeView).getEndRow();
				int viewStartColumn		= ((MyImageView) relativeView).getStartColumn();
				
				Log.e("test","start row:"+startRow);
				Log.e("test","end row:"+endRow);
				Log.e("test","column:"+startColumn);
				Log.e("test","view start row:"+viewStartRow);
				Log.e("test","view end row:"+viewEndRow);
				Log.e("test","view column:"+viewStartColumn);
				
				if((startRow>=viewStartRow && startRow<=viewEndRow) || (endRow>=viewStartRow && endRow<=viewEndRow)){
					if(startColumn==viewStartColumn){
						return false;
					}
				}
				
			}
		}
		
		return true;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	
}
