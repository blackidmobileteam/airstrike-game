package com.example.airstrikeexample.bean;

public class StrategyObject {
	int startRow,endRow,startColumn,endColumn;
	
	public StrategyObject(){
		
	}
	
	public StrategyObject(int sRow,int eRow,int sColumn,int eColumn){
		startRow	= sRow;
		endRow		= eRow;
		startColumn	= sColumn;
		endColumn	= eColumn;
	}

	public int getStartRow() {
		return startRow;
	}

	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}

	public int getEndRow() {
		return endRow;
	}

	public void setEndRow(int endRow) {
		this.endRow = endRow;
	}

	public int getStartColumn() {
		return startColumn;
	}

	public void setStartColumn(int startColumn) {
		this.startColumn = startColumn;
	}

	public int getEndColumn() {
		return endColumn;
	}

	public void setEndColumn(int endColumn) {
		this.endColumn = endColumn;
	}
	
	
}
