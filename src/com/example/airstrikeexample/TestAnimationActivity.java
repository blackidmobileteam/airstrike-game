package com.example.airstrikeexample;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class TestAnimationActivity extends Activity{
	
	ImageView animationImg;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.animation_activity);
		
		animationImg	= (ImageView)findViewById(R.id.imageView1);
		Animation hyperspaceJumpAnimation = AnimationUtils.loadAnimation(this, R.anim.test_animation);
		hyperspaceJumpAnimation.setAnimationListener(new AnimationListener() {
			
			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				animationImg.setVisibility(View.GONE);
			}
		});
		animationImg.startAnimation(hyperspaceJumpAnimation);
		
	}

}
