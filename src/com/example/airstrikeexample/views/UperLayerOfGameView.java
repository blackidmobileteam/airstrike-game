package com.example.airstrikeexample.views;

import com.example.airstrikeexample.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class UperLayerOfGameView extends View{
	
	public UperLayerOfGameView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(attrs);
	}
	
	private void init(AttributeSet attrs) { 
	    TypedArray a=getContext().obtainStyledAttributes(
	         attrs,
	         R.styleable.UperLayerOfGameView);

	    //Don't forget this
	    a.recycle();
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		
		switch(event.getAction()){
			case MotionEvent.ACTION_DOWN:
				
				break;
		}
		return super.onTouchEvent(event);
	}
	
}
