package com.example.airstrikeexample.views;

import com.example.airstrikeexample.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class GameView extends View{
	
	int mMaxX;
	int mMaxY;
	int xStart,xEnd;
	
	int boxWidth;
	int boxHeight;
	
	int counter=0;
	
	int touchXpos=0,touchYpos=0;
	
	boolean isFirstTime = true;
	
	public GameView(Context context,AttributeSet attr) {
		super(context,attr);
		init(attr);
		// TODO Auto-generated constructor stub
		
	}
	
	private void init(AttributeSet attrs) { 
	    TypedArray a=getContext().obtainStyledAttributes(
	         attrs,
	         R.styleable.GameView);

	    //Don't forget this
	    a.recycle();
	}
	
	public int getBoxSize(){
		return boxHeight;
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		// TODO Auto-generated method stub
		//super.onSizeChanged(w, h, oldw, oldh);
		mMaxX		= w;
		mMaxY		= h;
		xStart		= (mMaxX-mMaxY)/2;
		
		boxWidth	= (h-20)/8;
		boxHeight	= (h-20)/8;
		
		mMaxY		= (boxHeight*8)+10;
		xEnd		= xStart+(mMaxY-10);
		//Log.i("width","Width:"+w);
		Log.i("Height","Height:"+h);
		
	}
	
	public void setOnRedraw(){
		this.invalidate();
	}
	
	public int getXStart(){
		return xStart;
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		drawSqure(canvas);		
	}
	
	public void drawSqure(Canvas canvas){
		Path path1 = new Path();
		Paint p1 = new Paint();
		p1.setAntiAlias(true);
		p1.setColor(Color.LTGRAY);
		p1.setStyle(Paint.Style.STROKE); 
		p1.setStrokeWidth(1);
		
		int i = 0;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             ;
		int yStratFrom = 10;
		
		while(yStratFrom<=mMaxY){
			path1.moveTo(xStart, yStratFrom);
			path1.lineTo(xEnd, yStratFrom);
			canvas.drawPath(path1, p1);
			i++;
			yStratFrom	= 10+(boxHeight*i);
		}
		
		int j = 0;
		int xStartFrom = xStart;
		
		while (xStartFrom<=xEnd) {
			path1.moveTo(xStartFrom, 10);
			path1.lineTo(xStartFrom, mMaxY);
			canvas.drawPath(path1, p1);
			j++;
			xStartFrom	= xStart+boxWidth*j;
		}
	}
	
	public int getColumn(int x){
		/*
		int column = 0;
		
		if(x<xStart+boxWidth){
	    	column		= 1;
	    }
		if(x>(boxWidth+xStart)){
	    	float d		= ((float)(x-xStart)/(boxWidth));
	    	//Log.i("float value",""+d);
	    	column		= (int)Math.ceil(d);
	    }
		*/
		int column = 0;
		if(x>=(xStart+boxWidth)){
			float c = (x-xStart)/boxWidth;
			Log.e("test","column value"+c);
			column	= Math.round(c);
		}else if(x>xStart && x<(xStart+boxWidth)){
			column	= 0;
		}else{
			column	= -1;
		}
		
		return column;
	}
	
	public int getRow(int y){
		int row = 0;
		/*
		if(y<boxHeight && y>10){
	    	row			= 1;
	    }
		
		if(y>(boxHeight+10)){
			float d		= ((float)(y-10)/(boxWidth));
	    	//Log.i("float value",""+d);
	    	row		= (int)Math.ceil(d);
	    }*/
		
		if(y>=(10+boxWidth)){
			float r = (y-10)/boxWidth;
			Log.e("test","row value"+r);
			row		= Math.round(r);
		}else if(y>=10 && y<(10+boxWidth)){
			row		= 0;
		}else{
			row		= -1;
		}
		
		return row;
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		
	    switch(event.getAction()){
	    	case MotionEvent.ACTION_DOWN:
	    		/*
	    		if(x>xStart && x<xEnd){
	    			touchXpos = x;
		    		touchYpos = y;
		    		counter = 1;
		    		this.invalidate();
	    		}
	    		*/
	    		break;
	    }
	    
	    return true;
	}
	
	public boolean isTouchInBox(int x,int y){
		if(x>=xStart && x<=xEnd && y>=10 && y<=mMaxY){
			return true;
		}else{
			return false;
		}
	}
	
	public boolean isImageInBox(int x,int y,int index){
		
		int imgXStart = x-(boxWidth/2);
		int imgYStart = y-((boxHeight*(2+index))/2);
		
		if(imgXStart>=xStart && imgXStart<=xEnd && imgYStart>=10 && imgYStart<=mMaxY){
			return true;
		}else{
			return false;
		}
		
		/*if(x>=(xStart+(boxHeight/2)) && x<=(xEnd-(boxHeight/2)) && (y-(boxHeight/2))>=10 && (y+(boxHeight/2))<=(mMaxY)){
			return true;
		}else{
			Log.e("test","X start:"+xStart);
	    	Log.e("test","X end:"+xEnd);
	    	Log.e("test","Max Y:"+mMaxY);
	    	
			Log.e("test","droped image x:"+x);
	    	Log.e("test","droped image x+size:"+(x+boxHeight));
	    	Log.e("test","droped image Y:"+y);
	    	Log.e("test","droped image Y+size:"+(y+boxHeight));
			return false;
		}*/
	}
}
