package com.example.airstrikeexample.utils;

import android.content.Context;
import android.widget.ImageView;

public class MyImageView extends ImageView{
	
	Context context;
	int bitmapIndex;
	int startRow,endRow,startColumn,endColumn;
	
	
	public MyImageView(Context context,int index) {
		super(context);
		// TODO Auto-generated constructor stub
		this.context	= context;
		bitmapIndex		= index;
	}

	public int getBitmapIndex() {
		return bitmapIndex;
	}


	public void setBitmapIndex(int bitmapIndex) {
		this.bitmapIndex = bitmapIndex;
	}

	public int getStartRow() {
		return startRow;
	}

	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}

	public int getEndRow() {
		return endRow;
	}

	public void setEndRow(int endRow) {
		this.endRow = endRow;
	}

	public int getStartColumn() {
		return startColumn;
	}

	public void setStartColumn(int startColumn) {
		this.startColumn = startColumn;
	}

	public int getEndColumn() {
		return endColumn;
	}

	public void setEndColumn(int endColumn) {
		this.endColumn = endColumn;
	}
	
}
